const amqplib = require('amqplib');

const createAMQPClient = async (server, exchange, topic) => {
  const _callbacks = [];

  const connection = await amqplib.connect(`amqp://${server}`);
  const channel    = await connection.createChannel();

  await channel.assertExchange(
    exchange,
    'topic',
    { durable: false }
  );

  const queueOk = await channel.assertQueue('', { exclusive: true });
  await channel.bindQueue(queueOk.queue, exchange, topic);

  channel.consume(
    queueOk.queue,
    (msg) => _callbacks.forEach((cb) => cb(msg)),
    { noAck: true }
  );

  return {
    onMessage: (callback) => {
      _callbacks.push(callback);
    },
  };
};

module.exports = {
  createAMQPClient,
};

