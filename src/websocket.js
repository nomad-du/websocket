const socketIo = require('socket.io');

const initIo = (http) => {
  const io = socketIo(http);

  io.on('connection', (socket) => {
    console.log('Connection');

    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
  });

  return {
    sendUpdate: (userId, user) => {
      console.log('broadcast update', userId, user);
      io.sockets.emit('update', userId, user);
    },
  };
};

module.exports = initIo;
