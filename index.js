const app  = require('express')();
const http = require('http').Server(app);
const dotenv = require('dotenv');

dotenv.config();

const PORT = (() => {
  if (!process.env.PORT) {
    throw new Error('Need PORT');
  }

  return process.env.PORT;
})();

const AMQP_SERVER = (() => {
  if (!process.env.AMQP_SERVER) {
    throw new Error('Need AMQP_SERVER');
  }

  return process.env.AMQP_SERVER;
})();

const AMQP_EXCHANGE = (() => {
  if (!process.env.AMQP_EXCHANGE) {
    throw new Error('Need AMQP_EXCHANGE');
  }

  return process.env.AMQP_EXCHANGE;
})();

const AMQP_PATTERN = (() => {
  if (!process.env.AMQP_PATTERN) {
    throw new Error('Need AMQP_PATTERN');
  }

  return process.env.AMQP_PATTERN;
})();

const websocket = require('./src/websocket')(http);

const run = async () => {
  try {
    const { createAMQPClient } = require('./src/amqp');
    const amqpClient = await createAMQPClient(
      AMQP_SERVER,
      AMQP_EXCHANGE,
      AMQP_PATTERN
    );

    amqpClient.onMessage((msg) => {
      const { userId, update } = JSON.parse(msg.content);
      console.log('receive update', update);
      websocket.sendUpdate(userId, update);
    });
  } catch(e) {
    throw e;
  }
};

http.listen(PORT, () => {
  console.log(`listening on *:${PORT}`);
});

run();
